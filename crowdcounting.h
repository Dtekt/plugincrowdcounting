#ifndef CROWDCOUNTING_H
#define CROWDCOUNTING_H

#include <QObject>
#include "plugininterface.h"

class CrowdCounting : public QObject, public PluginInterface
{
    Q_OBJECT
    Q_INTERFACES(PluginInterface)
public:
    explicit CrowdCounting();
    void prepareGUI(MainWindow * win) const;
    void detectAndDisplay(MainWindow *win, FrameInfo *frameInfo) const;
};

#endif // CROWDCOUNTING_H
