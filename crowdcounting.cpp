#include "crowdcounting.h"
#include "ui_mainwindow.h"
#include <QQueue>
#include <QDateTime>

CrowdCounting::CrowdCounting()
{
    pluginName = "Crowd Counting";
    pluginDescription = "This plugin counts and display number of individuals present in the scene";
}

void CrowdCounting::prepareGUI(MainWindow * win) const
{
    win->ui->menuPlugins->addAction(pluginName);
    win->ui->crowdCountLabel->setEnabled(true);
    win->ui->crowdCount->setEnabled(true);
    win->ui->logView->appendPlainText(pluginName + " plugin loaded..\n" + QDateTime::currentDateTime().toString() + "\n");
}

void CrowdCounting::detectAndDisplay(MainWindow *win, FrameInfo *frameInfo) const
{
    int crowdCount = frameInfo->pedestriansVector.size();
    win->ui->crowdCount->display(crowdCount);
}

Q_EXPORT_PLUGIN2(crowdcounting,CrowdCounting)
